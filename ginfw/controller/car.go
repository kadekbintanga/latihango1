package controller

import(
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
	"encoding/json"
	"GoPro/ginfw/repository"
	"fmt"
)

func GetCar(c *gin.Context){
	car_id := c.Query("car_id")
	caridint, err := strconv.Atoi(car_id)
	fmt.Println(caridint)

	if caridint == 0{
		c.AbortWithStatusJSON(http.StatusBadRequest, map[string]interface{}{
			"message": "car_id can't be null",
		})
		return
	}

	cars, err := repository.GetCar(caridint)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, map[string]interface{}{
			"message": "Something went wrong boss",
		})
		return
	}

	responseData := map[string]interface{}{
		"payload": cars,
	}

	c.JSON(http.StatusOK, responseData)
}

func CreateCar(c *gin.Context){
	var req repository.Car
	err := c.ShouldBind(&req)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, map[string]interface{}{
			"message": "Something went wrong Boss",
		})
		return
	}
	cars,_ := repository.GetCar(0)
	req.CarId = len(cars) + 1

	err = repository.CreateCar(&req)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, map[string]interface{}{
			"message": "Something went wrong Boss",
		})
		return
	}
	responseData := map[string]interface{}{
		"payload": req,
	}
	c.JSON(http.StatusOK, responseData)
}

func writeJsonResponse(w http.ResponseWriter, status int, payload interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(payload)
}