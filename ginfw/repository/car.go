package repository

import(
	"fmt"
)

type Car struct{
	CarId	int	`json:"car_id"`
	Name	string	`json:"name"`
	Price	string	`jason:"price"`
}


var Cars = []Car{
	{
		CarId: 1,
		Name: "Hyundai Creta",
		Price: "500000000",
	},
}

func CreateCar(car *Car) error{
	Cars = append(Cars, *car)
	return nil
}

func GetCar(car_id int)([]Car, error){
	if len(Cars) == 0{
		return nil, fmt.Errorf("No Data !")
	}

	if car_id !=0{
		var oneCar []Car
		if len(Cars) < car_id{
			return oneCar, fmt.Errorf("Invalid car_id")
		}
		oneCar = append(oneCar, Cars[car_id-1])
		return oneCar, nil
	}
	return Cars, nil
}