package ginfw

import(
	"GoPro/ginfw/controller"
	"github.com/gin-gonic/gin"
)

func Start() *gin.Engine{
	router := gin.Default()


	
	router.GET("/cars", controller.GetCar)
	router.POST("/cars", controller.CreateCar)

	return router
}