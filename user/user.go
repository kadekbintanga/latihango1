package user

import (
	"encoding/json"
	"fmt"
	"sync"
	"net/http"
)


type User struct{
	Nama string `json:"name"`
}

var registeredUserList []User


type UserService struct{
}

type UserIface interface{
	Register(u *User)
	GetUser(w http.ResponseWriter)
}


func NewUserService() UserIface{
	return &UserService{}
}

func (u *UserService) Register(user *User){
	registeredUserList = append(registeredUserList, *user)
}


func(u *UserService) GetUser(w http.ResponseWriter){
	var wg sync.WaitGroup
	wg.Add(len(registeredUserList))
	for _, value := range registeredUserList{
		PrintName(&wg,&w, value.Nama)
	}
	json, err := json.Marshal(registeredUserList)
	if err != nil{
		fmt.Println("error")
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(json)
}

func PrintName(wg *sync.WaitGroup,w *http.ResponseWriter, name string){
	fmt.Println(name)
	wg.Done()
}